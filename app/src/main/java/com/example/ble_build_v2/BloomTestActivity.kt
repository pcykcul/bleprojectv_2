package com.example.ble_build_v2

import android.annotation.SuppressLint
import android.bluetooth.*
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.omni.support.ble.core.IResp
import com.omni.support.ble.core.ISessionCall
import com.omni.support.ble.core.NotifyCallback
import com.omni.support.ble.profile.SimpleBleCallbacks
import com.omni.support.ble.protocol.bloom.model.BloomResult
import com.omni.support.ble.rover.CommandManager
import java.lang.Exception
import java.util.*

class BloomTestActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val appState: MainActivity = this.application as MainActivity


        appState.session.link.setOnBleOperationCallback( object : SimpleBleCallbacks(){
            override fun onDeviceConnected(device: BluetoothDevice) {
                super.onDeviceConnected(device)
                Log.d("connected","connected")
                if(appState.session.isConnect()){
                    try {
                        appState.session.call(CommandManager.bloomCommand.unlock()).execute()
                    }catch (e : Exception){
                        Log.d("exception",e.toString())
                    }
                }
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    device.connectGatt(appState,true,object : BluetoothGattCallback (){
                        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
                            super.onServicesDiscovered(gatt, status)
                            Log.d("testing","testing")
                            /*
                            if (gatt != null) {
                                for(x in gatt.services){
                                    Log.d("service", x.uuid.toString());
                                    for(y in x.characteristics){
                                        Log.d("characteristics", y.uuid.toString());
                                        appState.session.call(CommandManager.bloomCommand.unlock()).execute()
                                        for(z in y.descriptors){
                                            Log.d("descriptors", z.uuid.toString());
                                        }
                                    }
                                }
                            }*/
                            if (gatt != null) {
                                gatt.getService(UUID.fromString("6e400001-e6ac-a7e7-b1b3-e699bae80062"))
                                    .getCharacteristic(UUID.fromString("6e400003-e6ac-a7e7-b1b3-e699bae80062"))

                            }
                        }

                        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
                            super.onConnectionStateChange(gatt, status, newState)
                            if(newState == BluetoothGatt.STATE_CONNECTED){
                                Log.d("testing",newState.toString())
                                gatt?.discoverServices()
                            }
                        }
                    })
                }

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    device.setPin("1234".toByteArray())
                };
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                    device.createBond()
                }
            }

            override fun onBonded(device: BluetoothDevice) {
                super.onBonded(device)
                Log.d("bonnd","bond success")
            }

            override fun onBondingFailed(device: BluetoothDevice) {
                super.onBondingFailed(device)
                Log.d("failked","bond failed")
            }

            override fun onDeviceConnecting(device: BluetoothDevice) {
                super.onDeviceConnecting(device)
            }

            override fun onDeviceReady(device: BluetoothDevice) {
                super.onDeviceReady(device)
                Log.d("ready","ready")
                appState.session.call(CommandManager.bloomCommand.recv())
                    .subscribe(object : NotifyCallback<BloomResult> {

                        override fun onSuccess(
                            call: ISessionCall<BloomResult>,
                            data: IResp<BloomResult>
                        ) {
                            val result = data.getResult()
                            if (result != null) {
                                Log.d("data 2",result.data2.toString())
                                Log.d("=====", result.toString())
                                appState.session.call(CommandManager.bloomCommand.unlock()).execute()
                            }else{
                                Log.d("pota","gagu kotlin")
                            }
                        }
                    })

            }

            override fun onLinkLossOccurred(device: BluetoothDevice) {
                super.onLinkLossOccurred(device)
                Log.d("dawdwa","dwadwadaw")
            }

            override fun onServicesDiscovered(device: BluetoothDevice, optionalServicesFound: Boolean) {
                super.onServicesDiscovered(device, optionalServicesFound)
                /*
                for (service in optionalServicesFound) {
                    Log.d("service uuid", service.uuid.toString());
                    val service: BluetoothGattService = gatt?.getService(service.uuid) as BluetoothGattService
                    //gatt.device.setPin()
                }*/
            }

            override fun onBondingRequired(device: BluetoothDevice) {
                super.onBondingRequired(device)
                Log.d("bonding required","bonding required")
            }

            override fun onError(device: BluetoothDevice, message: String, errorCode: Int) {
                super.onError(device, message, errorCode)
                Log.d("error",message)
            }
        })


        appState.session.connect()

    }
}
