package com.example.ble_build_v2

import android.app.Application
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.content.Intent
import android.util.Log
import com.omni.support.ble.BleModuleHelper
import com.omni.support.ble.rover.CommandManager
import com.omni.support.ble.session.SimpleSessionListener
import com.omni.support.ble.session.sub.BloomSession
import java.util.*


class MainActivity : Application() {

    public lateinit var session: BloomSession

    companion object {
        var ctx: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        ctx = applicationContext
        BleModuleHelper.init(this)//kotlin

        session = BloomSession.Builder()
                .address("FF:80:13:FE:2A:4A").keyOrg(byteArrayOf(0x62, 0x00, 0xe8.toByte(), 0xba.toByte(), 0x99.toByte(), 0xE6.toByte(), 0xb3.toByte(), 0xb1.toByte(),
                        0xe7.toByte(), 0xa7.toByte(), 0xAc.toByte(), 0xe6.toByte(), 0x00, 0x00, 0x40, 0x6E)).build();

        session.setListener( object : SimpleSessionListener(){
            override fun onError(device: BluetoothDevice, message: String,
                                 errorCode: Int) {
                Log.d("error",message)
            }

            override fun onConnecting() {}
            override fun onConnected() {
                Log.d("xconnected","connected");
                //session.call(CommandManager.bloomCommand.setUnlockMode(0x10)).execute()
                //session.call(CommandManager.bloomCommand.unlock()).execute()
            }
            override fun onDisconnected() {
                Log.d("disconnected","disconnected");
            }
            override fun onDeviceNoSupport() {
            }
            override fun onReady() {
                Log.d("ready","ready");

            }


        } )


        val intent = Intent(this, BloomTestActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }
}